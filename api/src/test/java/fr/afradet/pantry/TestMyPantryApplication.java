package fr.afradet.pantry;

import org.springframework.boot.SpringApplication;

public class TestMyPantryApplication {

  public static void main(String[] args) {
    SpringApplication.from(MyPantryApplication::main).with(ContainerConfiguration.class).run(args);
  }
}
