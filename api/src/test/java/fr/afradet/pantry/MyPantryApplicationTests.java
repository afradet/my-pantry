package fr.afradet.pantry;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;

@SpringBootTest
@Import({ContainerConfiguration.class})
class MyPantryApplicationTests {

  @Test
  void contextLoads() {}
}
